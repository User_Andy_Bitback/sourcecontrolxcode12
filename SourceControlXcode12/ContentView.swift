//
//  ContentView.swift
//  SourceControlXcode12
//
//  Created by Andrew Slimming on 18/03/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! Xcode 12 Branch by AJS")
            .padding()
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
