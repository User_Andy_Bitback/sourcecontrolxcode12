//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Andrew Slimming on 18/03/2021.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
